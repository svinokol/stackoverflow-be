package io.piano.stackexchange.service.impl;

import io.piano.stackexchange.dto.question.Question;
import io.piano.stackexchange.dto.question.QuestionsSearchResponse;
import io.piano.stackexchange.service.StackOverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class StackOverflowServiceImpl implements StackOverflowService {

    public static final String QUESTIONS_SEARCH_API
            = "https://api.stackexchange.com/2.2/search/advanced" +
            "?order=desc&sort=activity&title=%s&site=stackoverflow&filter=!9Z(-wwYGT";

    private RestTemplate restTemplate;

    @Autowired
    public StackOverflowServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Question> findQuestionsByTitle(String title){
        QuestionsSearchResponse searchResponse
                = restTemplate.getForObject(
                        String.format(QUESTIONS_SEARCH_API, title),
                        QuestionsSearchResponse.class);
        return searchResponse.getItems();
    }

}
