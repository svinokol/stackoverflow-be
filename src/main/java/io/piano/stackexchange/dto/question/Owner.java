package io.piano.stackexchange.dto.question;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class Owner {

    private Integer reputation;

    @JsonAlias("user_id")
    private Long userId;

    @JsonAlias("user_type")
    private String userType;

    @JsonAlias("accept_rate")
    private Integer acceptRate;

    @JsonAlias("profile_image")
    private String profileImage;

    @JsonAlias("display_name")
    private String displayName;

    private String link;
}
