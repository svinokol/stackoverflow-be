package io.piano.stackexchange.dto.question;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.util.List;

@Data
public class Question {

    private String title;

    private String link;

    @JsonAlias("question_id")
    private Long questionId;

    @JsonAlias("is_answered")
    private Boolean isAnswered;

    private List<String> tags;

    private Owner owner;

    @JsonAlias("creation_date")
    private Long creationDate;

    @JsonAlias("last_activity_date")
    private Long lastActivityDate;

    @JsonAlias("last_edit_date")
    private Long lastEditDate;

    private String body;
}
