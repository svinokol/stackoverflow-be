package io.piano.stackexchange.controller;

import io.piano.stackexchange.dto.question.Question;
import io.piano.stackexchange.service.StackOverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/stackoverflow")
public class StackOverflowController {


    @Autowired
    private StackOverflowService stackOverflowService;

    @GetMapping("/questions")
    public ResponseEntity<List<Question>> findQuestionsByTitle(
            @RequestParam(value = "title", defaultValue = "", required = false) String title){
        return ResponseEntity.ok(stackOverflowService.findQuestionsByTitle(title));
    }
}
