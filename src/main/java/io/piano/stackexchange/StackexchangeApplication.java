package io.piano.stackexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StackexchangeApplication {

    public static void main(String[] args) {
        SpringApplication.run(StackexchangeApplication.class, args);
    }
}
