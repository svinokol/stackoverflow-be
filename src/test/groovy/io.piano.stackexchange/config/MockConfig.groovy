package io.piano.stackexchange.config

import io.piano.stackexchange.service.StackOverflowService
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate
import spock.mock.DetachedMockFactory


@Configuration
class MockConfig {
    def detachedMockFactory = new DetachedMockFactory()

    @Bean
    StackOverflowService stackOverflowService() {
        return detachedMockFactory.Stub(StackOverflowService);
    }

    @Bean
    RestTemplate restTemplate() {
        return detachedMockFactory.Mock(RestTemplate)
    }
}